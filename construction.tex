\section{\harisa: Zero-Knowledge \cpsnark for Batch Set-Membership}\label{sec:set-mem}
%\dimitris{General editorial comment: I am thinking of removing all "parse". Not sure yet.}
%\matteo{It may be okay if it's clear what happens anyway}

In this section we show the construction of a \cpsnark for the relation $\cprelsetmem$ defined in $\cref{sec:prel-cp-setmem}$, where: the accumulator is the classical RSA accumulator from \Cref{fig:rsa-acc} where the accumulated elements are prime numbers larger than the $2\secpar$-th prime ($1619$ for $\secpar = 128$), and the commitment scheme for the commit-and-prove functionality is the Pedersen scheme of Fig.~\ref{fig:ped-comm}.
In \ifFullVersion \cref{sec:arbitraryelements} \else the full version \fi we discuss how this construction can be easily extended to accumulate arbitrary elements via an efficient hash-to-prime function.

\subsection{RSA Accumulators with hiding witnesses}
\label{subsec:HidingWit}
We describe a method to turn a witness $W$ of an RSA accumulator into another witness that computationally hides all the elements $u_i$ it proves membership of. As discussed in \Cref{sec:technical-overview} this constitutes the first building block towards achieving a zero-knowledge membership proof for committed elements.

%An RSA accumulator witness does not hide the element of the set they are proving membership. For example one can ensure that $W$ is or not about any $x$ by simply using $\VfyMem(\pp, \acc, x, \wacc)$. Even worse, one can brute-force try all the elements of the set and identify the one corresponding to $W$.

%In order to obtain zero-knowledge on an RSA accumulator we need to mask the witness provided to the verifier. 
%We introduce a new masking method for witnesses which is ZKP-friendly. 
Let $\Primes_{n} = \{2,3,5,7, \ldots, p_n\}$ be the set of the first $n$ prime numbers. Our method relies on two main ideas.

First, prover and verifier modify the accumulator $\acc$ so as to contain the first $2\secpar$ primes by computing $\accmasked \gets \acc^{\left ( \prod_{p_i \in \Primes_{2 \secpar}} p_i \right )}$. Note, $\accmasked = g_?^{  \left ( \prod_{x_i \in \SET} x_i \right ) \cdot \left ( \prod_{p_i \in \Primes_{2 \secpar}} p_i \right )} = \Accum(\pp, S \cup \Primes_{2\secpar})$.

Second, we build a randomized witness for $X \subset S$ as the witness for $(X \cup P) \subset (S \cup \Primes_{2\secpar})$ where $P$ is a randomly chosen subset of $\Primes_{2\secpar}$.
In more detail, given $\accwit$, the prover computes $\accwitmasked$ as follows:
\begin{itemize}
	\item choose at random $2 \secpar$ bits $b_1, \ldots , b_{2 \secpar} \sample \bit$ and let $s \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{b_i}$ and $\bar{s} \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{1 - b_i}$.
	
	\item $\accwitmasked \gets \accwit^{\bar{s}} = g_?^{\left ( \prod_{x_i \in \SET \setminus X} x_i \right ) \cdot \left ( \prod_{p_i \in \Primes_{2 \secpar}} p_i^{1-b_i} \right )}$.
\end{itemize}
%, i.e., $\hat W \gets \PrvMem(\pp, S \cup \Primes_{2\secpar}, X \cup P)$
%Intuitively, for the randomization of a witness one chooses at random which primes from $\Primes_{2 \secpar}$ are to be included in the exponent.
Essentially, we have $s$ as the product of the randomly chosen primes, $\bar{s}$ as the product of the primes not chosen, and we denote with $p^* \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i$ the product of all the first $2 \secpar$ primes.
%In the following we denote $s \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{b_i}$ as the product of the sampled primes, $\bar{s} \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{1 - b_i}$ as the product of the primes not sampled, and $p^* \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i$ as the product of the first $2 \secpar$ primes. 
%We denote the bit-length of $p^*$ as $\psize = \lc \log(p^*) \rc$. 
Finally, by $\D_{2 \secpar}$ we denote the distribution of $\bar{s}$, according to the sampling method described above.
Note that $s \bar{s} = p^*$. Also, the new witness $\accwitmasked$ could be verified by checking $\accwitmasked^{s \prod_{x_i \in X} x_i} = \accmasked$.

Our first technical contribution is proving that this randomization is sufficient. More precisely, we use a computational assumption over groups of unknown order, called DDH-II, and we show that under DDH-II $\accwitmasked$ is computationally indistinguishable from a random $R \sample \grp_?$.
We stress that this hiding property holds only for the value $\accwitmasked$ {\em alone}, i.e., when the random subset of $\Primes_{2\secpar}$ is not revealed. As we show later, this is sufficient for our purpose as we can hide the integer $s$ in the same way as we hide the elements we prove membership of.

In the following section we state and explain the DDH-II assumption. In brief, this is a variant of the classical DDH assumption where the random exponents follow specific, not uniform, distributions. %We justify its hardness in the generic group model {\cite{EC:Shoup97, IMA:Maurer05}.
Next, we prove that under DDH-II $\accwitmasked$ is computationally indistinguishable from random.

\subsubsection{The DDH-II assumption} First, we state the DDH-II assumption, which is parametrized by a generator $\GGen_?(1^\secpar)$ of a group (of unknown order in our case) and by a well-spread distribution $\WS_{2 \secpar}$ (in our case $\D_{2 \secpar}$).
A distribution $\WS_{2 \secpar}$ with domain $\mathcal{X}_{2 \secpar}$ is called {\em well-spread} if $\Pr[X = x| X \sample \WS_{2 \secpar}] \leq 2^{-2 \secpar}$ for each $x \in \mathcal{X}_{2 \secpar}$ (Intuition: the  elements sampled from this distribution are ``sufficiently random'').

\begin{assumption}[DDH-II]
	Let  $\grp_? \gets \GGen_?(1^\secpar)$ and $g_? \sample \grp_?$. Let $\WS_{2 \secpar}$ be a well-spread distribution with domain $\mathcal{X}_{2 \secpar} \subseteq [1,\minord(\grp_?)]$. Then for any PPT $\adv$:
	$$\left | \Pr[\adv(g_?^{x}, g_?^{y}, g_?^{x y})=0]- \Pr[\adv(g_?^{x}, g_?^{y}, g_?^{t})=0] \right |=\negl$$
	where $x \sample \WS_{ 2 \secpar}$ and $y,t \sample [1, \maxord(\grp_?) 2^\secpar]$.\footnote{Since the order of the group is unknown, we cannot efficiently produce uniformly random elements with $y,t \sample [1,\ord(g_?)]$. However, $ y,t \sample [1,\maxord(\grp) 2^\secpar]$ still produces statistically close to uniform elements.}
\end{assumption}


Our distribution of interest $\D_{2 \secpar}$ can be shown well-spread: there are $2^{2\secpar}$ outcomes and are all distinct, $\bar{s} = \prod_{p_i \in \Primes_{2 \secpar}} p_i^{1 - bi}$ are distinct since they are different products of the same primes (no $p_i$ can be used twice). It follows that $\Pr[\bar{s} \sample \D_{\secpar}] = 1/2^{2 \secpar}$ for every $\bar{s}$.

\begin{remark}
The constraint that the domain should be in $[1,\minord(\grp_?)]$ is for the following reason: If a sampled $x$ is larger than $\ord(g_?)$ then in the exponent of $g_?^x$ a reduction modulo $\ord(g_?)$ will implicitly happen leading to a $g_?^x = g_?^{x'}$ for some $x' \neq x$. This can turn $g_?^x$ more frequently sampled, which can potentially help the adversary distinguish between $(g_?^{x})^y$ and $g_?^t$.
%\dimitris{I am not satisfied from this paragraph. To be changed.}
%\dario{I slightly revised it and turned it into a remark -- It looks quite good to me}
%\dimitris{Can we avoid this under the Low-Order assumption?}
%\dario{My feeling is no. Consider a distribution that exceeds the group order by polynomially many $N$ values, then the adversary that outputs 1 for the first $g_?^{1}...g_?^{N}$ should be a good distinguisher...}
\end{remark}

Different variants of DDH-II have been proven secure in the generic group model \cite{EC:Shoup97, IMA:Maurer05} for prime order groups~\cite{damgardshort,C:BarMaZha19}. We can prove it secure for groups of unknown order similarly with minor technical modifications related to GGM proofs in such groups~\cite{EC:DamKop02}. 
%\dimitris{We can include the proof in the appendix if there is time.}

\begin{remark}
	The need of an at least $2^{2 \secpar}$-large domain $\mathcal{X}_{2 \secpar}$ (and at most $2^{-2\secpar}$ probability) for $\secpar$ security parameter comes from well-known subexponential attacks on DLOG~\cite{pohlig1978improved,pollard1978monte}.
\end{remark}

\subsubsection{Security Proof of our hiding witnesses}

\begin{theorem}
	For any parameters $\pp \gets \Setup(1^\secpar)$, set $S$ (where $S \cap \Primes_{2 \secpar} = \emptyset$), $R \sample \GG_?$ and $\accwitmasked$ computed as described above it holds:
	$$	\left |	\Pr [ \adv(\pp, S, \accwitmasked)=0 ]  - \Pr [\adv(\pp, S, R)=0 ] \right | = \negl $$
	for any PPT  $\adv$, under the DDH-II assumption  for $\GG_?$ and $\D_{2\secpar}$.
\end{theorem}
\begin{proof}
	Call $\adv$  an adversary  achieving a non-negligible advantage $\epsilon$ above, i.e.
	$	\epsilon := \left |	\Pr [ \adv(\pp, S, \accwitmasked)=0 ]  - \Pr [\adv(\pp, S, R)=0 ] \right |$
	We construct an adversary $\advB$ against DDH-II that, using adversary $\adv$, gains the same advantage.
	$\advB$ receives $(\grp_?, g_?, g_?^{\bar{s}}, g_?^r, g_?^{b \bar{s} r + (1-b) t})$, where $\bar{s} \sample \mathcal{D}_{2\lambda}$ and $r, t \allowbreak  \sample [1,\maxord(\grp_?) 2^\secpar]$. Then it chooses arbitrarily an element $\elm$ and sets $\SET = \{\elm\}$, $\pp \gets (\grp_?, g_?^r)$ and $V = g_?^{b \bar{s} r + (1-b) t}$. $\advB$ sends $(\pp, \SET, V)$ to the adversary $\adv$, who outputs a bit $b^*$. Finally, $\advB$ outputs $b^*$.
	
	First, notice that $g_?^r$ is statistically close to a random group element of $\grp_?$, meaning that $\adv$ cannot distinguish $\pp$ from parameters generated by $\AccScm.\Setup(1^\secpar)$. Furthermore if $b =0 $ then $V$ is again a (statistically indistinguishable element from a) uniformly random group element of $\grp_?$ therefore $\Pr [ {\advB = 0  | b=0} ] =\allowbreak \Pr [ {\adv(\pp, \SET, R)=0} ]$. On the other hand, if $b = 1$ then $ V = g_?^{r \cdot \bar{s}}  = \accwitmasked_u $ is a witness of $u$ so $\Pr [ {\advB = 0  | b=1} ] = \Pr [ {\adv(\pp, \SET, \accwitmasked)=0} ]$. Therefore we conclude that the probability of $\advB$ to win the DDH-II is $\epsilon$.
\end{proof}

%\dimitris{Should we write the randomization process formally? Ie in a figure.}
%
%\dario{I think it is okay like this as we eventually have the full description of the CPSNARK prover}

\subsection{Building Blocks}
\subsubsection{Succinct proofs of knowledge of exponent (\PoKE)}
We recall the succinct proofs of knowledge of a DLOG for hidden order groups, introduced by Boneh et al.~\cite{C:BonBunFis19}. More formally, $\PoKE$ is a protocol for the relation 
\[ \relPoKE(A,B;x) = 1 \iff A^x =B\]
parametrized by a group of unknown order $\grp_?$ and a random group element $g_? \in \grp_?$. The statement consists of group elements $A,B \in \grp_?$ while the witness is an arbitrarily large  $x \in \ZZ$.

\ifShortVersion See \cite{C:BonBunFis19} or the full version for a description of the protocol. \fi
\ifFullVersion \Cref{fig:PoKE} gives a description of the protocol. \fi
For simplicity we directly expose its non-interactive version (after Fiat-Shamir). 
\ifFullVersion
Although the interactive version of the protocol is secure with $\secpar$-sized challenges its non-interactive version is only secure with $2\secpar$-sized challenges, due to a subexponential attack \cite{boneh2018survey}.
\begin{figure}[h]
	\begin{framed}
		\pseudocode[colspace]{
			\underline{\Setup(1^{\lambda}):}\\[]
			\t (\accGrp, g_?) \gets \GGen_?(1^\lambda) \\
			\t \pcreturn \crs \eqdef (\accGrp, g_?)
		}
		\bigskip
		
		%\fbox{
		%			\begin{pcvstack}%[center]
		\pseudocode[colspace]{
			\underline{\CPProve\left( \crs, A, B ; x  \right):}\\
			%
			%			\t \text{Parse } \crs \text{ as } (\accGrp, g_?) \\
			\t \ell \gets \Hprime(\crs, A, B)\\
			\t Q \gets A^{\lf \frac{x}{\ell} \rf}, \res \gets x \mod{\ell} \\
			\t \pcreturn \pi = (Q, \res) 
		}
		\bigskip
		
		\pseudocode[colspace]{
			\underline{\CPVerPf\left( \crs, A, B, \pi \right):}\\
			%
			%			\t \text{Parse } \crs \text{ as } (\accGrp, g_?) \\
			\t \text{Parse } \pi \text{ as } (Q, \res) \\
			\t \ell \gets \Hprime(\crs, A, B) \\
			\t \text{Reject if } A, B, Q \notin \grp_? \text{ or } \mathit{res} \notin [0,\ell-1] \\
			\t \text{Reject if } Q^{\ell} A^{\res} \neq B 
		}
	\end{framed}
	\caption{The succinct argument of knowledge \PoKE~\cite{C:BonBunFis19}. $\Hprime$ denotes a cryptographic hash function that outputs a prime of size $2\secpar$, modeled as a random oracle.}
	\label{fig:PoKE}
\end{figure}

\begin{remark}
	We note that the proof of \cref{fig:PoKE} is not originally secure for arbitrary bases $A$, but rather for random ones. For arbitrary bases extra care should be taken, that give a proof of additional $2$ group elements. We wil show that the protocol still suffices for our needs, since we combine it with a \snark for the relation $\res = x \mod{\ell}$. In a nutshell, a $\PoKE$ for random bases with a \snark for $\res = x \mod{\ell}$ give a succinct proof of knowledge of exponent for arbitrary bases.
\end{remark}
\fi
This protocol is succinct: proof size and verifier's work are independent of the size of $x$, $O(\secpar)$ and $O(\| \ell \|) = O(\secpar)$ respectively.



\subsubsection{\cpsnark for integer arithmetic relations}
We assume an efficient \cpsnark $\cpZKModarith$ for the following relation:
\[\relmodarithm(\cmx{\cm_{\vec u}}, \cmx{\cm_{s,r}}, \clg, \ell, \hat{k}) = 1 \iff\]
\[  \hat{k} = s \cdot h \cdot \prod_{i\in[m]} u_i  +r \mod{\ell}\]
Above, $\vec u = (u_1, \ldots, u_m) \in \ZZ^m$ is a vector of integers with a corresponding multi-integer commitment $\cm_{\vec u}$; $r,s \in \ZZ$ are integers committed with a corresponding multi-integer commitment $\cm_{s,r}$ and $\ell, \clg\in \ZZ$, $\hat{k} \in [0,\ell-1]$ are (small) integers known as public inputs by both prover and verifier.

The above relation is equivalent to the integer relation:
\[\relarithm(\cmx{\cm_{\vec u}}, \cmx{\cm_{s,r}}, \clg, \ell, \hat{k}; q) = 1 \iff q \ell + \hat{k} = s \cdot h \prod_i u_i +r \]
In fact this is how a modulo operation is encoded in a SNARK circuit. $q$ here is a witness given to the SNARK.\footnote{For the sake of our general protocol, it is not necessary that $q$ remains hidden. It is only important that the proof is succinct w.r.t. its size. However, $\vec u$, $s$ and $r$ should remain hidden.}

\subsubsection{\cpsnark for inequalities}
We need a \cpsnark $\cpZKBound$ for the relation (where $B$ is a public integer):
\[ \cprelBound (\cmx{\cm_{\vec u}}, B) = 1 \iff  \bigwedge_{i \in [n]} u_i > B   \]


\subsection{Our Construction for Batched Set Membership (\harisa)}
Here we describe our \cpsnark for the relation $\cprelsetmem$ for RSA accumulators and Pedersen commitments to vectors of integers.
Let us recall the setting in more detail.

Prover and verifier hold an accumulator $\acc$ to a set $\SET$ and a commitment $\cm_{\vec u}$. The set's domain are prime numbers greater than $ p_{2\secpar}$, the $2\secpar$-th prime. The protocol works in the ``trusted accumulator model'' (\cref{sec:trusted-acc}), which means the set is assumed to be public but the verifier does not take it as an input, it only uses $\acc$, for efficiency reasons.\footnote{This is a common consideration in scalable systems. The accumulator to the set is either computed once by the verifier or validated by an incentivized majority of parties that is supposed to maintain it.}
%We assume a public accumulator digest $\acc$, in the "Trusted Accumulator Model" (see sec.~\ref{}), meaning that $\acc$ is honestly computed for a set $\SET$, which contains elements in right domain (natural primes greater than $p_{2 \secpar}$). The set is assumed to be public, but we assume that the verifier does not take it as input, for efficiency reasons. Then the batch of set elements $\vec u = (u_1, \ldots , u_m)$ are vector-committed as integers via a pedersen commitment (see sec.~\ref{}).

The prover knows a batch of set elements $\vec u = (u_1, \ldots , u_m)$ that are an opening of the commitment $\cm_{\vec u}$, and its goal is to convince the verifier that all the $u_i$'s are in $\SET$.
To this end, we assume that the prover has an accumulator witness $\accwit_{\vec u}$ as an input, either precomputed or given by a witness-providing entity. In this sense, the prover's goal translates into convincing the verifier that it has $\accwit_{\vec u}$ such that $\accwit_{\vec u}^{\prod_{i} u_i} = \acc$ (see also \cref{rem:wit-model} where we further refine this setting).

We give a full description of the \cpsnark in \Cref{fig:our-scheme}. We refer to the technical overview (sec.~\ref{sec:technical-overview}) for a high-level explanation. Below we provide additional comments.

To begin with, both prover and verifier transform the accumulator $\acc$ into $\accmasked$, the one corresponding to the same set with the additional small prime numbers from $\Primes_{2 \secpar}$.\footnote{This operation can also be precomputed, we make it explicit only to show that they can both work with a classical RSA accumulator as an input.}
Next, the prover transforms $\accwit$ into a hiding witness as $\accwitmasked = \accwit^{\bar{s}}$ via our masking method of \cref{subsec:HidingWit}, and then computes a (Fiat-Shamir-transformed) zero-knowledge $\Sigma$-protocol for the accumulator's verification $\accwitmasked^{s u^*} = \accmasked$. However, since the last message $k$ of the protocol is not succinct, it computes a $\PoKE$ for the relation $(\accmasked^h R) = (\accwitmasked_{\vec u})^k$ (exponent $k$), which is the verification equation of the $\Sigma$-protocol.
The $\PoKE$ verification requires a check $Q^{\ell} \accwitmasked_{\vec u}^{\hat{k}}$ where $\hat{k}$ is supposed to be $k \mod \ell$.
The last step of the proof is to show that $\hat{k}$ is not just ``some exponent'' but it is exactly $r + h s u^* \mod \ell$ with $u^*$ being the product of all the $u_i$'s committed in $\cm_{\vec u}$.
To do so, the prover generates a proof with the $\cpZKArith$  \cpsnark over the commitments $\cm_{\vec u}$, $\cm_{s,r}$ ($r$ is the masking randomness of the $\Sigma$-protocol sampled in the first move). Also, for soundness we require that $s$ and $r$ are committed {\em before} receiving the random oracle challenge $h$.
Finally, the prover generates a proof with $\cpZKBound$ over the commitment $\cm_{\vec u}$ to ensure that the elements are in the right domain.\footnote{For the sake of generality we present $\pi_2, \pi_3$ as distinct proofs. In practice they can be proved by the same \cpsnark and save on proof-size.}
%\begin{remark}
%	For the protocol to be sound we require that $s$ and $r$, the randomness used in the first move of the $\Sigma$-protocol, are committed before the prover receives the challenge $h$.
%\end{remark}

We present our construction in \cref{fig:our-scheme}. This construction is obtained by applying Fiat-Shamir in the random oracle model (ROM) and additional optimizations to its interactive counterpart which we describe in \ifFullVersion the appendix (\cref{fig:our-scheme-interactive}).\else the full version.\fi

\medskip
\begin{figure}[h]
	\begin{framed}
		\raggedright
%		\centering
%		\setstretch{1.25}
		\pseudocode[colspace]{
				\underline{\Setup\left( 1^\lambda, \ck, \pp \right):}\\[]
				\t \crs_2 \gets \cpZKModarith.\Setup(1^\lambda, \ck, \relmodarithm)\\
				\t \crs_3 \gets \cpZKBound.\Setup(1^\lambda, \ck, \cprelBound)\\
				\t \pcreturn \crs \eqdef (\ck, \pp, \crs_2, \crs_3)
		}
\bigskip

%	
		%\fbox{
			%			\begin{pcvstack}%[center]
%	
		
%				\setstretch{1.25}
		\pseudocode[colspace]{
			\underline{\CPProve\left( \crs, \acc, \cm_{\vec \elm}; \accwit_{\vec \elm}, \vec \elm, \opn_{\vec \elm}  \right):}\\[]
			%
			\t \accmasked \gets \acc^{\prod_{p_i \in \Primes_{2\secpar}}p_i} \ifFullVersion \\
%			\t \text{Parse } \crs \text{ as } (\ck, \pp_\AccScm, \crsarith, \crsBound) \\
			\else; \fi 
			\t \text{Let } u^* = \prod_i u_i, \, p^* = \prod_{p_i \in \Primes_{2\secpar}}p_i \\
			\t \text{Sample } b_1, \ldots , b_{2 \secpar} \sample \bit\\ 
			\t \text{Let } s \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{b_i}, \; \bar{s} \eqdef \prod_{p_i \in \Primes_{2 \secpar}} p_i^{1-b_i} \ifFullVersion \\ \else ; \fi
			\t \accwitmasked_{\vec u} \gets \accwit_{\vec u}^{\bar{s}}\\
			%\t \text{Sample } r \sample \bit^{\| p^* \| + \| u^* \| + 3\secpar} 
			%\\ \ho { \secpar for hash and 2 \secpar for integer sigma protocol} \\
			\t \text{Sample } r \sample \bit^{\| p^* \| + \| u^* \| + 2\secpar} \\
%			\ho {$r$ > $l$ is enough} \\
			\t \cm_{s,r} \gets \Comm_\ck(s,r; \opn_{s,r}) \\
			\t R \gets \accwitmasked_{\vec u}^{ r } \ifFullVersion \\ \else ; \fi
			\t \clg \gets \Hash(\crs || \acc || \cm_{\vec \elm} || \cm_{s,r} || \accwitmasked_{\vec u} || R) \ifFullVersion \\ \else ; \fi
			\t k \gets r + (\elm^* s) \clg  \\
			\t \pi_1 \gets \ZK^\PoKE.\Prv \big ( (\grp_?, g_?), \accwitmasked_{\vec u}, \accmasked^{\clg}R;k \big ) \ifFullVersion \\ \else ; \fi
			\t \text{Parse } \pi_1 \text{ as } (Q, \hat{k})\\
			\t \ell \gets \Hprime((\grp_?, g_?), \accwitmasked_{\vec u}, \accmasked^{h}R) \\
			\t \pi_2 \gets \cpZKModarith.\Prv(\crs_2, \cm_{\vec u}, \cm_{s,r}, \clg, \ell, \hat{k}; \vec \elm, \opn_{\vec \elm}, r, s, \opn_{s,r}) \\
			\t \pi_3 \gets \cpZKBound.\Prv(\crs_3, \cm_{\vec u}, p_{2 \secpar}; \vec u, \opn_{\vec u}) \\
			%					\t \pcreturn \pi = \big ( \accwitmasked_{\vec u}, \cm_{s,r}, \clg, \pi_1, \pi_2, \pi_3 \big )
			\t \pcreturn \pi = \big ( \accwitmasked_{\vec u}, R, \cm_{s,r}, \pi_1, \pi_2, \pi_3 \big )
		} % end pseudocode
	\bigskip
	
		%				\pcvspace[0pt]
		%				\pseudocode[colspace]{
			%					\> \textbf{Verifier's checks} \> \\
			%					\> \bullet \t x \> 
			%				}
		%			\end{pcvstack}
	%		\begin{pcvstack}%[center]
	
	
	%\setstretch{1.25}
	\pseudocode[colspace]{
		\underline{\CPVerPf\left( \crs, \acc, \cm_{\vec \elm}, \pi \right):}\\[]
		%
		\t \accmasked \gets \acc^{\prod_{p_i \in \Primes_{2\secpar}} p_i} \\
		%					\t \text{Parse } \crs \text{ as } (\ck, \pp_\AccScm, \crsmodarith, \crsBound) \\
		%\t \text{Parse } \pi \text{ as }  ( \accwitmasked, \cm_{s,r}, \clg, \pi_1, \pi_2, \pi_3  ) \\
		\t \text{Parse } \pi \text{ as }  ( \accwitmasked_{\vec u}, R, \cm_{s,r}, \pi_1, \pi_2, \pi_3  ) \text{ and } \pi_1 \text{ as } (Q,\hat{k}) \\
%		\t \text{Parse } \pi_1 \text{ as } (Q, \res)\\
		\t \ell \gets \Hprime((\grp_?, g_?), \accwitmasked_{\vec u}, \accmasked^{h}R) \\
		\t \clg \gets \Hash(\crs || \acc || \cm_{\vec \elm} || \cm_{s,r} || \accwitmasked_{\vec u} || R) \\
		%\t \text{Let } R \gets \frac{\accwitmasked^k}{\acc^\clg} \\ %\frac{\accwitmasked^k}{\acc^\clg} \\
		%\t \text{Reject if } \clg \neq \Hash(\crs || \acc || \cm_{\vec \elm} || \cm_{s,r} || \accwitmasked_{\vec u} || R) \\
		\t \text{Reject if } \ZK^\PoKE.\Vfy(\grp_?, g_?), \accwitmasked_{\vec u}, \accmasked^{\clg}R,\pi_1) \neq 1\\
		\t \text{Reject if } \cpZKModarith.\Vfy(\crs_2, \cm_{\vec u}, \cm_{s,r}, \clg, \ell, \hat{k}, \pi_2) \neq 1 \\
		\t \text{Reject if } \cpZKBound.\Vfy(\crs_3, \cm_{\vec u}, p_{2 \secpar}, \pi_3) \neq 1
		%					\> \> \t \text{Accept if all are true:} \\
		%					\> \> \t\t \bullet \Rgrp \acc^\clg = \accwitmasked^k  \\
		%					\> \> \t\t \bullet \CP_k.\Vfy(\ck, \cm_u, \cm_r, \cm_s, \clg, \pi)
		%
		%				\\[5pt][\hline]
	} % end pseudocode
	%				\pcvspace[0pt]
	%				\pseudocode[colspace]{
	%					\> \textbf{Verifier's checks} \> \\
	%					\> \bullet \t x \> 
	%				}
	%		\end{pcvstack}
	%} % end fbox
	\end{framed}
	\caption{\harisa: our scheme for proving set membership of a committed element. We let $H$ denote a cryptographic hash function modeled as a random oracle.
	%(\semin{Some representations of $\crs$ are different from each function. I think it would be better to make it $\crs_2$, $\crs_3$.})
%	\ho { Do we need $z$ and $Q'$? If we know $k$ itself then we don't need them. $k$ is already in $\cpZKModarith$ and extractable by SNARK. }
	}
	\label{fig:our-scheme}
\end{figure}
\vspace*{-10pt}

\begin{theorem}
\label{thm:construction-security}
	Let $\Hash, \Hprime$ be modeled as random oracles and $\cpZKModarith$, $\cpZKBound$ be secure \cpsnarks. The construction in \cref{fig:our-scheme} for the relation $\cprelsetmem$ is a secure \cpsnark: succinct, knowledge-sound under the adaptive root assumption, and zero-knowledge under the DDH-II assumption.
\end{theorem}
\begin{proof}
	For succinctness, one can inspect that the proof size is proportional to that of $\cpZKArith$ and $\cpZKBound$ plus some small constant overhead. Similarly for the verifier's cost. So succinctness is inherited from succinctness of $\cpZKArith$ and $\cpZKBound$.
	
	The proof for its interactive version \ifFullVersion (\cref{fig:our-scheme-interactive}) is in the appendix, \cref{thm:construction-security-interactive}. \else appears in the full version of the paper. \fi Then knowledge-soundness and zero-knowledge come directly from the (tight) security of the Fiat-Shamir transformation for constant-round protocols~\cite{attema2021fiat}, in the random oracle model.
\end{proof}

\medskip
\noindent
\textit{Extensions.} In \ifFullVersion Appendix \ref{apx:extensions} \else the full version \fi we show how to extend our \cpsnark to support arbitrary---not necessarily prime---set elements, using an efficient hash-to-prime proof based on a single hash execution. We also discuss how to extend our protocol to prove (in zero-knowledge) batch non-membership.