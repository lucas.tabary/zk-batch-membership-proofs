\section{Background}
\label{sec:preliminaries}

%\dimitris{For S\&P I'd rename the section to "background".}
We give informal definitions for the main cryptographic primitives used in our constructions.

\subsection{Commitments}
Commitment schemes allow one to commit to a value, or a collection of values (e.g., a vector), in a way that is \textit{binding}---a commitment cannot be opened to two different values---and \textit{hiding}---a commitment leaks nothing about the value it opens to.
In our work we also consider commitment schemes that are \textit{succinct}, meaning informally that the commitment size is fixed and shorter than the committed value.
Here is a brief description of the syntax we use in our work:
$\ComSetup(\secparam) \to \ck$ returns a commitment key $\ck$;
$\Comm(\ck, x; \opn) \to \cm$ produces a commitment $\cm$ on input a value $x$ and randomness $\opn$ (which is also the opening).


\subsection{(Commit-and-Prove) SNARKs}

%\subsubsection*{\NIZKs.}
%\matteo{Move to a model with urs.}
%In this work we use and assume \textit{transparent} \NIZKs, i.e. whose algorithms use a reference string $\crs$ sampled uniformly.
\begin{definition}[\snark]
	A \snark for a relation family $\relfam = \{\relfam_\lambda\}_{\lambda\in\naturals}$ is a tuple of algorithms $\ZK = (\Setup, \CPProve, \allowbreak \CPVerPf)$ with the following syntax:
	\begin{itemize}
		%	\item $\Pi.\CPKG(\rel) \to \k \eqdef (\ek, \vk)$
		\item $\ZK.\Setup(1^\lambda, \rel) \to \crs$ outputs a relation-specific common reference string $\crs$.
		\item $\ZK.\CPProve(\crs, \inp, \wit) \to \pi$ on input $\crs$, a statement $\inp$ and a witness $\wit$ such that $\rel(\inp,\wit)$, it returns a proof $\pi$.
		\item $\ZK.\CPVerPf(\crs, \inp, \pi) \to b \in \bit$ on input $\crs$, a statement $\inp$ and a proof $\pi$, it accepts or rejects the proof.
	\end{itemize}
	
	We require a \snark to be complete, knowledge-sound and succinct. Completeness means that for any $\lambda \in \naturals, \rel \in \relfam_\lambda$ and $(\inp,\wit) \in \rel$ it holds with overwhelming probability that $\CPVerPf(\crs, \allowbreak \inp, \allowbreak \pi)=1$ where $\crs \gets \Setup(1^\lambda, \rel)$ and proof $\pi \gets \CPProve(\crs, \allowbreak \rel, \allowbreak \inp, \allowbreak \wit)$. Knowledge soundness informally states we can efficiently ``extract'' a valid witness from a proof that passes verification. Succinctness means that proofs are of size $\poly$ (or sometimes ${\sf poly}(\secpar, \log |\wit|)$) and can be verified in time $\poly{\sf poly}(|\inp|+\log|\wit|)$. %\dimitris{Isn't ${\sf polylog}( \log |\wit|)$ redundant?} %FIXED
	A \snark may also satisfy zero-knowledge, that is the proof leaks nothing about the witness (this is modeled through a simulator that can output a valid proof for an input in the language without knowing the witness). In this case we call it a \zksnark.
%	We use variants of these notions with certain composability properties, e.g. requiring auxiliary inputs and relation generators. For a full formal treatment of these, we refer the reader to Sections 2.2 and 2.5 in \cite{benarroch2021zero}. \dimitris{I wouldn't put this as the reference here... Maybe even remove the last sentence?}
Whenever the relation family is obviously defined, we talk about a ``\snark for a relation $\rel$''.
\end{definition}

\subsubsection{Commit-and-Prove \snarks (\cpsnarks)}
We use the framework for black-box modular composition of commit-and-prove \snarks (or \cpsnarks) in \cite{CCS:CamFioQue19} and \cite{benarroch2021zero}.
Informally a  \cpsnark is a \snark that can efficiently prove properties of inputs committed through some commitment scheme $\CommScm$. In more detail, a \cpsnark for a relation $\rel_\text{inner}(\inp; u, \omega)$ is a \snark that for a given commitment $\cm$ can prove knowledge of $\wit \eqdef (u,\omega, \opn)$ such that $\cm = \Comm(u;\opn)$ and $\rel_\text{inner}(\inp; u, \omega)$ holds.
We can think of $\omega$ as a non-committed part of the witness. In a \cpsnark, besides the proof, the verifier's inputs are $\inp$ and $\cm$.

%Examples of \cpsnarks include... \matteo{TODO} \dimitris{Do we need this?}

\begin{remark}[Syntactic Sugar for \snarks/\cpsnarks]
For convenience we will use the following notational shortcuts. We make explicit what the private input of the prover is by adding semicolon in a relation and in a prover's algorithm (e.g., $\rel(\inp; \wit)$). We explicitly mark relations as ``commit-and-prove'' by a tilde. We leave the assumed commitment scheme implicit when it's obvious from the context. Occasionally, we will also explicitly mark the commitment inputs by squared box around them (e.g. $\cmx{\cm_u}$) and we will assume implicitly that the relation includes checking the opening of these commitments (and we will not make explicit the openings). We assume that in the commitment $\cmx{c_u}$ the subscript $u$ defines the variable $u$ the commitment opens to. Analogously the opening for $c_u$ is automatically defined as $\opn_u$. Example: $\cprel_\ck(\cmx{c_u}, h; r) = 1 \iff h = \mathsf{SHA}256(u || r)$ is a shortcut for $\cprel_\ck(c_u, h; r, u, \opn_u) = 1 \iff h = \mathsf{SHA}256(u || r) \wedge c_u = \Comm(\ck, u; \opn_u)$.
\end{remark}

\subsubsection{Modular \snarks through \cpsnarks}\label{sec:composing-cpsnarks}
%\dimitris{Is this subsubsection relevant? Where do we compose \cpnizks?}
We use the following folklore composition of (zero-knowledge) \cpsnarks (cf.~\cite[Theorem 3.1]{CCS:CamFioQue19}).
Fixed a commitment scheme and given two \cpsnarks $\CP_1, \CP_2$ respectively for two ``inner'' relations $\cprel_1$ and $\cprel_2$, we can build a  (CP) \snark for their conjunction (for a shared witness $u$) $\cprel^*(\cmx{\cm_\cwit},\inp_1,\inp_2; \omega_1,\omega_2) = \rel_1(\cmx{\cm_\cwit}, \inp_1; \omega_1) \wedge \rel_2(\cmx{\cm_\cwit}, \inp_2; \omega_2)$ like this:
the prover commits to $\cwit$ as $\cm_\cwit \gets \Comm(\cwit, \opn)$; generates proofs $\pi_1$ and  $\pi_2$ from the respective schemes; outputs combined proof $\pi^* \eqdef (\cm_\cwit, \pi_1, \pi_2)$.
The verifier checks each proof over respective inputs $(\inp_1, \cm_\cwit)$ and $(\inp_2, \cm_\cwit)$, with shared commitment $\cm_\cwit$.

%The following theorem (informally stated) is a direct consequence of Theorem 3.1 in \cite{CCS:CamFioQue19}.
%
%\begin{theorem}[Black-Box Composition of \cpsnarks]
%	\label{thm:snarks-from-cp-comp}
%	The construction above is a secure \zksnark for the conjunction relation $\rel^*$.
%\end{theorem}

\subsection{Accumulators to Multisets}
A multiset is an unordered collection of values in which the same value may appear more than once. We denote by $S_1 \uplus S_2$ the union of multisets $S_1$ and $S_2$, i.e., the multiset $S_3$ where the multiplicity of any $x \in S_3$ is the sum of its multiplicity in $S_1$ and $S_2$.
For two multisets $S_2 \subset S_1$, $S_1 \uminus S_2$ denotes the multiset difference of $S_1$ and $S_2$, i.e., the multiset $S_3$ where the multiplicity of any $x \in S_3$ is the multiplicity of $x$ in $S_1$ minus the multiplicity of $x$ in $S_2$.

Cryptographic accumulators \cite{EC:BenDeM93} are succinct commitments to sets that also allow one to generate succinct proofs of membership (and sometimes also non-membership). In our work we use accumulators that enjoy three additional properties. First, they support multisets. Second, they are {\em dynamic}, meaning that from an accumulator to $S_1$ one can publicly compute an accumulator to $S_1 \uplus S_2$ without knowing $S_1$. Third, one can create succinct membership proofs for batches of elements, i.e., to prove that $X \subset S$.
In more detail, such an accumulator is a tuple of algorithms $\AccScm = (\Setup, \allowbreak \Accum, \allowbreak  \PrvMem, \allowbreak  \VfyMem, \allowbreak  \AccAdd)$ such that:
\begin{itemize}[leftmargin=10pt]
\item $\Setup(\secparam) \to \pp$ generates public parameters;
\item $\Accum(\pp, S) \to \acc$ outputs  accumulator $\acc$ for a multiset $S$;
\item $\PrvMem(\pp, S, X) \to \wacc_{X}$ outputs a membership proof  ($X \subset S$);
\item $\VfyMem(\pp, \acc, X, \wacc_X) \to 0/1$ accepts or rejects a membership proof $\wacc_X$;
\item $\AccAdd(\pp, \acc, S') \to \acc'$ computes  accumulator  to $S \uplus S'$.
\end{itemize}
A multiset accumulator is secure if any PPT adversary has negligible probability of creating a valid membership proof for a multiset $X \not\subset S$, namely to output a tuple $(S, X, \wacc)$ such that there is an $x \in X$ such that $x \notin S$ and $\VfyMem(\pp, \Accum(\pp, S), X, \wacc_X)=1$. 
%\dimitris{Shall we also include remove?}
%\dario{I'd say we can avoid it, it does not seem essential, plus RSA accumulators don't have an efficient deletion method}

We note that the popular RSA accumulator \cite{EC:BarPfi97,C:CamLys02,ACNS:LiLiXue07,C:BonBunFis19} enjoys all the properties mentioned above.

\subsection{Relations for batch set-membership and set-insertion}\label{sec:relmem}\label{sec:relins}
Our focus in this work is on building efficient \cpsnarks for the following two relations parametrized by an accumulator scheme $\AccScm$ and parameters $\pp_\AccScm$:
%\[ \relsetmem(\acc; U, \accwit) =1 \iff \AccScm.\VfyMem(\pp_\AccScm, \acc, U, \accwit) = 1 \]
%\[ \relsetupd(\acc, \acc'; U) =1 \iff \AccScm.\AccAdd(\pp_\AccScm, \acc, U) = \acc' \]
\[ \cprelsetmem(\cmx{\cm_{U}}, \acc; \accwit) \!= \!1\! \iff \! \AccScm.\VfyMem(\pp, \acc, U, \accwit) \! = \! 1 \]
\[ \cprelsetupd(\cmx{\cm_{U}}, \acc, \acc') =1 \iff \AccScm.\AccAdd(\pp_\AccScm, \acc, U) = \acc' \]
In a nutshell, a \cpsnark for $\cprelsetmem$ can prove that $\cm_U$ is a commitment to a vector of values such that each of them is in the multiset accumulated in $\acc$. A \cpsnark for $\cprelsetupd$ can instead prove that $\acc'$ is a correct update of the accumulator $\acc$ obtained by inserting the elements committed in $\cm_{U}$.
For the relation $\cprelsetupd$ we are not interested in obtaining proofs that are zero-knowledge (i.e., so as to hide $U$), as the $\AccAdd$ algorithm is deterministic and thus simply having public accumulators $\acc, \acc'$ may leak information on the added elements.

\label{sec:trusted-acc}
The specific notion of knowledge soundness we assume for \cpsnarks for these relations is the one where the malicious prover is allowed to select an arbitrary set $S$ to be accumulated but the accumulator $\acc$ is computed honestly from $S$.
%   requires a set-membership-specific tweak on the standard notion: a malicious adversary should be able to  select an arbitrary set, but the accumulator over that set should be computed honestly.
Given an accumulator scheme $\AccScm$, we informally talk about this specific notion as ``security under the Trusted Accumulator-Model for $\AccScm$''. We do not provide formal details since this model corresponds to the notion of partial-extractable soundness in Section 5.2 in \cite{benarroch2021zero}\footnote{We notice that their model uses a slightly different language and formalizes accumulators as (binding-only) commitments for commit-and-prove  \NIZKs.}; we refer the reader to this work for further details.

This trusted accumulator model fits several applications where the accumulator is maintained by the network.

On the other hand, we stress that in the $\relsetupd$ relation, the trusted accumulator assumption is assumed only for $\acc$ but {\em not} for $\acc'$. The interesting implication of this is that one can view a \cpsnark for $\relsetupd$ as a means to move from a trusted accumulator $\acc$ to a trustworthy one $\acc'$. Thinking of $\acc_0$ as the accumulator to the empty set that everyone knows and can efficiently compute, $\relsetupd$  allows certifying the generation of an accumulator to any multiset.

The next sections  show some  interesting byproducts of having modular commit-and-prove \snarks for  relations $\cprelsetmem$ and $\cprelsetupd$.

\subsubsection{Composing (commit-and-prove) set-membership relations}
\label{sec:prel-cp-setmem}
The advantage of having \cpsnarks for the set-membership relation (rather than just \snarks) is that one can use the composition of \cref{sec:composing-cpsnarks} to obtain {\em efficient} \zksnarks for proving properties of elements in an accumulated set, e.g., to show that $\exists U=\{u_1, \ldots, u_n\}$ such that a property $P$ holds for $U$ (say, every $u_i$ is properly signed) and $U \subset S$, where $S$ is accumulated in some $\acc$.
In particular, such a \zksnark can be obtained via the simple and efficient composition of a \cpsnark for $\cprelsetmem$ (like the ones we construct in our work) and any other \cpsnark for $P$.
% by focusing on a commit-and-prove variant of $\relsetmem$ and composing them with other commit-and-prove relations. 
%With this goal in mind, we focus on constructing a \cpsnark for the (commit-and-prove) relation:
%\[ \cprelsetmem(\cmx{c_U}, \acc; \accwit) = 1 \iff \AccScm.\VfyMem(\pp_\AccScm, \acc, U, \accwit) = 1 \]

%\subsection{Commit-and-Prove  for Set Membership}
%\matteo{TODO}

\subsubsection{From set-insertion to \multiswap}\label{sec:ins2mswap}
Ozdemir et al. \cite{USENIX:OWWB20} introduce an operation over (RSA) accumulators called \multiswap. Consider two multisets $S$ and $S'$ and a sequence of pairs $(x_1, y_1), \ldots, (x_n, y_n)$, where each pair represents {\em in order} a ``swap'', namely removal of $x_i$ and insertion of $y_i$.
Verifying a \multiswap means checking that $S'=S_n$ where $S_0=S$ and $S_{i} = S_{i-1} \uminus x_i \uplus y_i$.
\cite{USENIX:OWWB20} shows that this check can be reduced to
\[
\exists S_{mid}: S_{mid} = S \uplus \{y_i\}_i \wedge S_{mid} = S' \uplus \{x_i\}_i
\]
So, when using accumulators, \multiswap can be represented via the following relation:
\[
\relmswap(\acc, \acc'; X, Y) =1 \iff  
\] 
\[
\exists \acc_{\text{mid}}: \relsetupd(\acc, \acc_{\text{mid}}; Y) \wedge \relsetupd(\acc', \acc_{\text{mid}} ; X)
\]
Thus, a \cpsnark for $\relmswap$ can be obtained via the (self)composition of a \cpsnark for $\relsetupd$.

\subsubsection{Chaining \multiswap}
Consider a scenario where an accumulator evolves in time, namely at time $i$ a user returns a new accumulator $\acc_{i}$ along with a proof $\pi_i$ that $(\acc_{i-1}, \acc_{i}) \in \relmswap$ (and possibly additional proof that the elements added/removed satisfy a certain property, e.g., in Rollup they are valid transactions). It is easy to see that the concatenation  $(\pi_1, \acc_1, \ldots, \acc_{n-1}, \pi_n)$ constitutes a proof for $(\acc_0, \acc_{n}) \in \relmswap$.

\subsection{Building blocks}
%We describe two concrete building blocks used in our protocols.

\subsubsection{Pedersen Commitments of Integer values}
The \cpsnarks we construct are defined for commitments generated using the classical extension of Pedersen commitments to vectors. In particular, we sometimes use a variant of this scheme for committing to integers (instead of field elements); we describe it in \cref{fig:ped-comm}.
We assume a prime $p$ and an algorithm $\GGen_p$ that generates appropriate parameters for groups of order $p$.
Since we commit to an integer $x$ whose size is potentially larger than $p$ we split the integer into several ``chunks'', of size $\Cchunksize \leq p$ specified in the parameters, and then we apply the standard vector-Pedersen on this split representation. 
%As a special case, when committing to field elements in $\ZZ_p$, we can assume they will ``fit'' in a single generator $g$.
We let the setup algorithm take as input a bound $\Cbound$ denoting the max integer that we can commit to. 
The construction is perfectly hiding, and computationally binding under the discrete logarithm assumption.

\newcommand{\mycodesize}{\footnotesize}

\begin{figure*}[b]
	\begin{framed}
	\centering
%	\setstretch{1.25}
	\subfloat[RSA Accumulator for multisets of prime numbers. Above $\mathsf{Prod}(S)$ denotes the integer product of the elements in $S$.\label{fig:rsa-acc}]
{
%	\begin{pcvstack}[center]
			\pseudocode[colspace,codesize=\mycodesize]{
				\underline{\Setup(1^{\lambda}):}\\[]
		 (\accGrp, g_?) \gets \GGen_?(1^\lambda) \\
		 \pcreturn \pp \eqdef (\accGrp, g_?)
			\quad  % artificial space
	}
\hfill
\hspace{10pt}
\pseudocode[colspace,codesize=\mycodesize]{
		\underline{\mathsf{Accum}(\pp, S):}\\[]
		\mathsf{prd} \gets \mathsf{Prod}(S)\\
		\pcreturn \acc \eqdef  g_?^\mathsf{prd}
}
\hfill
\hspace{10pt}
\pseudocode[colspace,codesize=\mycodesize]{
	\underline{\AccAdd(\pp, \acc, S'):}\\[]
	\mathsf{prd}' \gets \mathsf{Prod}(S')\\
	\pcreturn \acc' \eqdef \acc^{\mathsf{prd}'}
}
\hfill
\hspace{10pt}
\pseudocode[colspace,codesize=\mycodesize]{
	\underline{\PrvMem(\pp, S, X):}\\[]
	\mathsf{prd} \gets \mathsf{Prod}(S), \,
	\mathsf{prd}_{X} \gets \mathsf{Prod}(X)\\
	%\t z := \mathsf{prd}/\mathsf{prd}_{X} \\
	\pcreturn \wacc \eqdef g_?^{\mathsf{prd}/\mathsf{prd}_{X}} 
	\qquad  % artificial space
}
\hfill
\hspace{10pt}
\pseudocode[colspace,codesize=\mycodesize]{
	\underline{\VfyMem(\pp, \acc, X, \wacc):}\\[]
		\mathsf{prd}_{X} \gets \mathsf{Prod}(X)\\
		\text{Accept iff } \wacc^{\mathsf{prd}_{X}} = \acc 
}
}
\bigskip
\smallskip

 \subfloat[Pedersen Commitments for vectors of integers\label{fig:ped-comm}. $\Cbound$ is an upper bound over the integers we can commit to. $\Cchunksize$ is the size of the chunks in which we divide each integer. $\Cnelems$ is the number of integers we can commit at the same time. $m = \ceil{\frac{\Cbound}{\Cchunksize}}$ is the number of chunks needed for each integer.]
{
			\pseudocode[colspace, codesize=\mycodesize]{
		\underline{\Setup(1^{\lambda}, 
			\Cbound \in \naturals, 
			\Cchunksize \in \naturals, 
			\Cnelems \in \naturals):}\\[]
		\t (\grp_p, f) \gets \GGen_p(1^\lambda); \;
		\text{If } \Cchunksize  > p \text{ then output } \bot \\
		\t \text{Let } \Cchunks \eqdef  \Cnelems \cdot \ceil{\frac{\Cbound}{\Cchunksize}} \\
		\t \text{Sample } g_1, \ldots , g_{\Cchunks}, h \sample \grp_p\\
		\t \pcreturn \ck \eqdef \left( \grp, \Cbound, \Cchunksize, \Cnelems, g_1, \ldots , g_{\Cchunks}, h \right)
			\qquad\quad  % artificial space
	}
	\hfill
	\hspace{20pt}
	\pseudocode[colspace,codesize=\mycodesize ]{
		\underline{\Comm(\ck, \vec{x} \in \ZZ^{\Cnelems}; r \in \ZZ_p):}\\[]
		\t \text{If } \exists i : x_i >  \Cbound \text{ then output } \bot \\
		\t \text{Let } \left( x^{(i)}_1, \dots, x^{(i)}_m \right) \text{ be }
		 \text{ the representation of } x_i \text{ in base } \Cchunksize \text{ for } i \in [\Cnelems]\\
		\t \vec{y} := \left (x^{(1)}_1 , \dots , x^{(1)}_{m}  , \dots , x^{(\Cnelems)}_{1} , \dots , x^{(\Cnelems)}_{m} \right );
%		\t \vec{y} := \left ( (x^{(1)}_j)_{j \in [m]} , \dots , (x^{(\Cnelems)}_{j})_{j \in [m]} \right )\\
		\t \pcreturn h^r \prod_{i = 1}^N g_i^{y_{i}}
	}
}
\end{framed}
\caption{Accumulator and commitment schemes we will use throughout this work}
\label{fig:comm-accs}
\end{figure*}

\subsubsection{RSA Accumulators}
Another crucial component of our \cpsnarks are RSA accumulators to multisets \cite{EC:BarPfi97,C:BonBunFis19}, that we recall in \cref{fig:rsa-acc}.
In particular, we assume their instantiation over any group of unknown order (including, e.g., classical RSA groups or class groups \cite{buchmann2011survey}) whose parameters are generated by an algorithm $\GGen_?$ and over which the Strong RSA \cite{EC:BarPfi97} and the Adaptive Root \cite{EC:Wesolowski19} assumptions hold. We recall that for these Accumulators the set elements should be primes (or hashed-to-primes if not).
%The construction is secure under \matteo{TODO}.

%\dimitris{Should we first present plain RSA accumulators and then our modification or directly the modification? I think the modification is part of our contributions so it should be in next sections.}


%\subsection{DDH-II assumption} \dimitris{Is this the right place?}
%\dario{I would move it right before the theorem/lemma that will use it}

