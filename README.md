# Variations on ZK Batch Membership Proofs

A collection of content related to the "Succinct ZK Batch Proofs for Set
Accumulators"; including:
- a sketch of a first variation, using the new number-theoretic assumption
  and/or modified DDH-II assumption;
- another variation, based on the old randomization method;
- ideas for batch non-membership proofs.
